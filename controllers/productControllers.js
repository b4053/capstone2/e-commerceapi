const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//ADD NEW PRODUCTS
module.exports.addItem = (reqBody) => {
	return Product.findOne({ serialNum: reqBody.serialNum }).then(result => {
		if(result === null) {
			let newItem = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				model: reqBody.model,
				serialNum: reqBody.serialNum,
				firmware: reqBody.firmware,
				quantity: reqBody.quantity,
				price: reqBody.price
			})
			return newItem.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return {message:'New item has been added.'};
				}
			})
		} else {
			return {message:'Item already exists.'}
		}
	})
};


//RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = (reqBody) => {
	return Product.find({ isActive: true }).then((result, err) => {
		if(err) {
			return false;
		} else {
			return result;
		}
	})
}


//RETRIEVE SPECIFIC PRODUCT
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then((result, err) => {
		if(result.isActive === true) {
			return result;
		} else {
			return {message: 'No item found.'}
		}
	})
}


//UPDATE SPECIFIC PRODUCT
module.exports.updateProduct = (productId, reqBody) => {
  	let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			model: reqBody.model,
			serialNum: reqBody.serialNum,
			firmware: reqBody.firmware,
			quantity: reqBody.quantity,
			price: reqBody.price
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((result,err) => {
		if(err) {	
			return false;
		} else {
			return {message: 'Item has been updated.'}
		}
	})
};


//CHANGE PRODUCT STATUS
// module.exports.notActive = (productId) => {
// 	return Product.findById(productId).then((result, error) => {
// 		if(error) {
// 			return false;
// 		}
//  		return isActive = false
// 		return result.save().then((notActive, saveErr) => {
// 			if(saveErr){
// 				return false;
// 			}else {
// 				return notActive;
// 			}
// 		})
// 	})
// };


//ARCHIVE SPECIFIC PRODUCT
module.exports.archiveProduct = (reqParams, reqBody) => {
	let archivedProduct = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams, archivedProduct).then((course, error) => {
		if(error) {
			return false;
		} else {
			return {message: 'Item has been archived.'}
		}
	})
};