const User = require('../models/User');
const Product = require('../models/Product');


//ENCRYPT PASSWORD
const bcrypt = require('bcrypt');
const auth = require('../auth');


//USER REGISTRATION
/*
STEPS:
1. CREATE A NEW USER OBJECT 
2. MAKE SURE THAT THE PASSWORD IS ENCRYPTED\
3. SAVE THE NEW USER TO THE DATABASE
*/
module.exports.registerUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then((user,error) => {
				if(error){
					return false;
				} else {
					return {message: "Registration complete."}
					// console.log("Registration complete.")
				}
			})
		} else {
			return {message: "User already exists."};
		}
	})
};

//CHECK DUPLICATE EMAILS
module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
};


//LOGIN - USER AUTHENTICATION
/*
STEPS:
1. CHECK THE DATABASE IF THE USER EMAIL EXIST
2. COMPARE THE PASSWORD PROVIDED IN THE LOGIN FORM WITH THE PASSWORD STORED IN THE DATABASE
3. GENERATE/RETUEN A JSON WEB TOKEN IF THE USER IS SUCCESSFULLY LOGED IN AND RETURN FALSE IF NOT
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return {message: "Incorrect email or password."}
		} else {
			const isPasswordCorrrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrrect){
				return { accessToken : auth.createAccessToken(result.toObject()) }
			} else {
				return {message: "Incorrect email or password."}
			}
		}
	})
};

//RETRIEVE SPECIFIC USER DETAILS
module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams).then((result, err) => {
		if(err) {
			return false;
		} else {
			return result
		}
	})
};


//RETRIEVE ALL USER DETAILS
module.exports.getAllUserDetails = (reqBody) => {
	return User.find({ isAdmin: false}).then((result, err) => {
		if(err) {
			return false;
		} else {
			return result
		}
	})
};


//CHANGE NORMAL USER TO ADMIN
module.exports.toAdmin = (id, res) => {
	let makeAdmin = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(id, makeAdmin).then((result, err) => {
		if(result) {
			return true;
		} else {
			return false;
		}
	})
};


//CREATE ORDER
module.exports.createOrder = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
	
		user.hasPurchased = true;

		user.orders.push({
			productId: data.productId,
			qty:data.qty,
			price: data.price * data.qty
		});
		return user.save().then((user, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
		})
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		
		product.quantity -= data.qty;

		product.customers.push({
			userId: data.userId,
			qtyOrdered: data.qty,
			price: data.price 
		});
		return product.save().then((product, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
		})
	});
		if(isUserUpdated && isProductUpdated){
			return true;
		} else {
			return false;
		}
};


// RETRIEVE ALL ORDERS
module.exports.getOrders = (reqBody) => {
	return User.find({ isAdmin: false, hasOrdered: true }).then((result, err) => {
		if(err) {
			return false;
		} else {
			return {
				orders: result
			}
		}
	})
};






































// module.exports.enroll = async (data) => {
// 	//ADD THE COURSEID TO THE ENROLLMENTS ARRAY OF THE USER
// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		//PUSH THE COURSE ID TO ENROLLMENTS PROPERTY
// 		user.enrollments.push({ courseId: data.courseId});
// 		//SAVE
// 		return user.save().then((user, error) => {
// 			if(error) {
// 				return false;
// 			} else {
// 				return true
// 			}
// 		})
// 	});

// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 		//ADD THE USERID IN THE COURSE'S ENROLLMENTS PROPERTY
// 		course.enrollees.push({ userId: data.userId});
// 		return course.save().then((user, error) => {
// 			if(error) {
// 				return false;
// 			} else {
// 				return true
// 			}
// 		})
// 	});

// 	//VALIDATION

// 	if(isUserUpdated && isCourseUpdated){
// 		return true;
// 	} else {
// 		return false;
// 	}
// };
