const mongoose = require("mongoose") ;

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		require: [true, "First name is required."]
	},

	LastName: {
		type: String,
		require: [true, "Last name is required."]
	},

	mobileNo: {
		type: String,
		require: [true, "Mobile number is required."]
	},

	email: {
		type: String,
		require: [true, "Email is required."]
	},

	password: {
		type: String,
		require: [true, "Password is required."]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			qty: {
				type: Number,
				required: [true, 'Order quantity is required.']
			},

			price: {
				type: Number,
				required: [true, 'Price is required.']
			
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});



module.exports = mongoose.model("User", userSchema);