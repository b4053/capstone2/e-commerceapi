const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({


	name: {
		type: String,
		required: [true, 'Product name is required.']
	}, 

	description: {
		type: String,
		required: [true, 'Product description is required.']
	},

	model: {
		type: String,
		required: [true, 'Product model is required.']
	},

	serialNum: {
		type: String,
		required: [true, 'Serial Number is required.']
	},

	firmware: {
		type: String,
		required: [true, 'Firmware Version is required.']
	},

	quantity: {
		type: Number,
		required: [true, 'Quantity is required.']
	},

	price: {
		type: String,
		required: [true, 'Price is required.']
	},

	isActive: {
		type: Boolean,
		default: true
	}, 

	createdOn: {
		type: Date,
		default: new Date()
	},

	customers: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required.']
			},
			qtyOrdered: {
				type: Number,
				required: [true, 'Quantity ordered is required']
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});




module.exports = mongoose.model('Product', productSchema);