const express = require("express");
const router = express.Router();
const auth = require("../auth");
const UserController = require("../controllers/userControllers");


//USER REGISTRATION
router.post('/register', (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


//CHECK DUPLICATE EMAILS
router.post('/checkEmail', (req,res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
});


//LOGIN
router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});


//RETRIEVE SPECIFIC USER DETAILS
router.get('/:id', (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		UserController.getUserDetails(req.params.id).then(result => res.send(result))
	} else {
		res.send(`You're not an admin.`)
	}

});


//RETRIEVE ALL USER DETAILS
router.get('/', (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin) {
		UserController.getAllUserDetails(req.body).then(result => res.send(result))
	} else {
		res.send(`You're not an admin.`)
	}
});


//CHANGE NORMAL USER TO ADMIN
router.put('/toAdmin/:id', (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	if (isAdmin) {
		UserController.toAdmin(req.params.id).then(result => res.send(result))
	} else {
		res.send(`You're not an admin.`);
	}
});


//CREATE ORDER
router.post('/createOrder', auth.verify, (req,res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		qty: req.body.qty,
		price: req.body.price
	}
	UserController.createOrder(data).then(result => res.send(result));
});


// RETRIEVE ALL ORDERS
router.get('/', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin) {
		UserController.getOrders(req.body).then(result => res.send(result))
	} else {
		res.send(`You're not an admin.`)
	}
});




























module.exports = router;




























//RETRIEVE ORDERS
router.get('/orders', auth.verify, (req,res) => {
	const order = auth.decode(req.headers.authorization);
	UserController.orders(order.id).then(result => res.send(result));
});
































// //ENROLL USER TO A COURSE

// router.post("/enroll", auth.verify, (req, res) => {
// 	let data = {
// 		userId : auth.decode(req.headers.authorization).id,
// 		courseId: req.body.courseId
// 	}

// 	UserController.enroll(data).then(result => res.send(result));
// })
















module.exports = router;