const express = require('express');
const router = express.Router();
const auth = require('../auth');

const ProductController = require('../controllers/productControllers');



//ADD NEW PRODUCT
router.post('/addItem', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		ProductController.addItem(req.body).then(result => res.send(result));
	} else {
		res.send(`You're not an admin.`)
	}
});


//RETRIEVE ALL PRODUCTS
router.get('/', (req, res) => {
	ProductController.getAllProducts(req.body).then(result => res.send(result))
});


//RETRIEVE SPECIFIC PRODUCT
router.get('/:id', (req, res) => {
	ProductController.getProduct(req.params.id).then(result => res.send(result))
});


//UPDATE SPECIFIC PRODUCT
router.put('/:productId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(`You're not an admin.`)
	}
});


//CHANGE PRODUCT STATUS
// router.put("/status/:productId", (req, res) => {
// 	ProductController.notActive(req.params.id).then(result => res.send(result))
// });


//ARCHIVE SPECIFIC PRODUCT
router.put('/archive/:productId', auth.verify, (req,res) => {
	const archived = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(archived.isAdmin){
		ProductController.archiveProduct(req.params.id, req.body).then(result => res.send(result));
	} else {
		res.send(`You're not an admin.`)
	}
});






module.exports = router;