const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");


//ROUTES
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// const orderRoutes = require("./routes/orderRoutes");

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));



app.use("/api/user", userRoutes);
app.use("/api/product", productRoutes);
// app.use("/api/order", orderRoutes);



mongoose.connect(process.env.DB_MONGODB, {
	useNewUrlParser: true,
	useUnifiedTopology: true 
})


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));



app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
});